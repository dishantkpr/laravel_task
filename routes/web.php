<?php

use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\ProposalController;
use App\Http\Controllers\TreeEntryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [TreeEntryController::class,'index']);
// Route::get('/', [CurrencyController::class,'index']);
// Route::get('/', [ProposalController::class,'index']);
Route::get('/', function ()
{
 return redirect('/login');
});
Route::get('/add_proposal', [ProposalController::class,'add']);
Route::post('/add_proposal',[ProposalController::class ,'store_proposal']);
Route::post('/search',[ProposalController::class ,'search']);
Route::get('/edit_proposal', [ProposalController::class,'edit']);
Route::get('/delete/{id}', [ProposalController::class,'delete']);
Route::get('/add_wallet', [CurrencyController::class,'add_wallet']);
Route::post('/add_wallet', [CurrencyController::class,'create_wallet']);

Auth::routes();
Route::middleware('auth')->group(function () {
Route::get('/home', [ProposalController::class,'index'])->name('home');

});
