<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Wallet;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function index()
    {
        $wallets=Wallet::with('currencies')->get();
      
       return view('home',['wallets'=>$wallets]);
    }

    public function add_wallet()
    {

        $currencies=Currency::get();
       return view('add_wallet',['currency'=>$currencies]);
    }

    public function create_wallet(Request $request)
    {

        $input=$request->all();
        $create_wallet=Wallet::create([
            'name'=>$input['name'],
            'mobile'=>$input['mobile'],
            'wallet_address'=>"",
            'currency'=>$input['currency']
        ]);
        // dd($input);
        return redirect('/');
    }
}
