<?php

namespace App\Http\Controllers;

use App\Models\TreeEntry;
use App\Models\TreeEntryLang;
use Illuminate\Http\Request;

class TreeEntryController extends Controller
{
    protected $output = '';
    protected $base_nodes = '';
    protected $final_arr = [];
    public function index()
    {
        $lang = "eng";
        $entries = TreeEntry::with(['relation_detail'=>function($q)
        use ($lang)
        {
            $q->where('lang',$lang);
        }])->get();
        $data = [];
        foreach ($entries as $entry) {
            $data[$entry['entry_id']]['name'] = $entry->relation_detail->name;
            $data[$entry['entry_id']]['parent_entry_id'] = $entry->parent_entry_id;
        }
        $this->arrayTree($data, 0);
        $this->base_nodes .= "<ul class='parent'><details><summary>Task Details</summary>";
        foreach ($data as $key => $value) {
            if ($value['parent_entry_id'] == 0) {
                $this->base_nodes .= '<li id="detail_' . $key . '" > <summary>' . $value['name'] . ' </summary><details class="details_tag" data-value="' . $value['name'] . '" data-id="' . $key . '"><summary>Load</summary></details></li>';
            }
        }
        $this->base_nodes .= "</details></ul>";
        return view('welcome', ['details' => $this->output, 'ajax' => $this->base_nodes]);
    }

    public function arrayTree($tree_array, $parent_entry, $step = 0, $last_step = -1, $tag = 0)
    {
        foreach ($tree_array as $key => $value) {
            if ($parent_entry == $value['parent_entry_id']) {
                if ($step > $last_step) {
                    if ($step == 0 && $tag == 0) {
                        $this->output .= "<ul class='parent'><details><summary>Task Details</summary>";
                    } else {
                        $this->output .=  "<ul ><details><summary>+</summary>";
                    }
                }

                if ($step == $last_step) {
                    $this->output .=  "</li>";
                }

                $this->output .=  "<li> <summary>" . $value['name'] . " </summary>";
                if ($step > $last_step) {
                    $last_step = $step;
                }
                $step++;
                //Recursion Calling the same function Again
                $this->arrayTree($tree_array, $key, $step, $last_step);
                $step--;
            }
        }
        if ($step == $last_step) {
            $this->output .=  "</li></details></ul>";
        }
    }

    public function api_request(Request $request)
    {
        // API for get Child by Root Node ID
        $input = $request->all();

        $node_id = $input['node_id'];
        $lang = "eng";
        $entries = TreeEntry::with('relation_detail')->get();
        $data = [];
        foreach ($entries as $entry) {
            $data[$entry['entry_id']]['name'] = $entry->relation_detail->name;
            $data[$entry['entry_id']]['parent_entry_id'] = $entry->parent_entry_id;
        }
        $this->arrayTree($data, $node_id, 0, -1, 1);

        return $this->output;
    }
}
