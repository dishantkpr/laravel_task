<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TreeEntry extends Model
{
    use HasFactory;
    protected $table='tree_entry';
    protected $fillable=[
        'entry_id',
        'parent_entry_id'
    ];

    public function relation_detail(){
        return $this->hasOne('App\Models\TreeEntryLang', 'entry_id','entry_id');
    }
}
