<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable=[
         'name',
         'mobile',
         'wallet_address',
         'currency',
    ];

    function currencies(){
       return $this->hasOne('App\Models\Currency','id','currency');
    }
    
}
