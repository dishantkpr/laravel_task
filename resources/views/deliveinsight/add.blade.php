<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Proposal</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body>
    <div class="container">
      <center>
        <h1>Request for Proposal</h1>

        {{-- <h3>Add Wallet</h3> --}}
        <a href="/"><button class="btn btn-primary">Back to Home</button></a>
       <form method="post" action="/add_proposal" enctype="multipart/form-data">
        <div class="row">
            <div class="col-6">
                <label>Company Name</label><br>
                <input type="text" placeholder="Company Name" class="form-control" name="name" required><br>
            </div>
            <div class="col-6">
                <label>Contact name</label><br>
                <input type="text" class="form-control" placeholder="Contact Name" name="contact_name" required><br>
                @csrf
            </div>
        </div>
        <div class="row">
            
            <div class="col-6">
                <label>Phone Number</label><br>
                <input type="number" class="form-control" placeholder="Mobile" name="mobile" required><br>
                @csrf
            </div>
            <div class="col-6">
                <label>Email</label><br>
                <input type="email" placeholder="Email" class="form-control" name="email" required><br>
            </div>
        </div>

        <div class="row">
            
            <div class="col-6">
                <label>Due date</label><br>
                <input type="date" class="form-control" placeholder="YYYY-MM-DD" name="due_date" required><br>
                @csrf
            </div>
            <div class="col-6">
                <label>Attachment</label><br>
                <input type="file" class="form-control" name="file" required><br>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <label>Captcha</label><br>
            </div>
            <div class="col-2">
                <input type="number" class="form-control" name="num1" readonly="true" value="{{ rand(1,50) }}"> 
            </div>
            <div class="col-1">
              +
            </div>
            <div class="col-2">
                <input type="number" class="form-control" name="num2" readonly="true" value="{{ rand(1,50) }}"> 
            </div>
            <div class="col-1">
                =
            </div>
            <div class="col-2">
                <input type="number" class="form-control" name="solution" required> 
            </div>
            <div class="col-2">
                
                <input type="submit" class="btn btn-success" />
            </div>
           
        </div>
       
        
       </form>
    <center>
    </div>
</body>
</html>