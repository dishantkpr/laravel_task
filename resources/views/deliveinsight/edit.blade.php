<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Proposal</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body>
    <div class="container">
      <center>
        <h1>Request for Proposal</h1>

        {{-- <h3>Add Wallet</h3> --}}
        <a href="/"><button class="btn btn-primary">Back to Home</button></a>
       <form method="post" action="/add_wallet">
        <label>Name</label><br>
        <input type="text" placeholder="Name" class="form-control" name="name"><br>
        <label>Mobile</label><br>
        @csrf
        <input type="number" class="form-control" placeholder="Mobile" name="mobile"><br>
        <label>Select Currency</label><br>
        <select class="form-control" name="currency">
            @foreach ( $currency as $cur)
                 <option value="{{$cur->id}}">{{ $cur->symbol." ".$cur->name }}</option>
            @endforeach
        </select>
        <input type="submit" class="btn btn-success" />
        
       </form>
    <center>
    </div>
</body>
</html>